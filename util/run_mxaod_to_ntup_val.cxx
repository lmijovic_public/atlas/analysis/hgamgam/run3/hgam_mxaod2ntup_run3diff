//#include "hgam_mxaod2ntup_run3diff/mxaod_to_ntup_val.h"

#include "HGamAnalysisFramework/RunUtils.h"
#include "AnaAlgorithm/AnaAlgorithmConfig.h"

#include <xAODRootAccess/Init.h>

int main(int argc, char *argv[]) {

  xAOD::Init().ignore();

  // Create our algorithm con 
  EL::AnaAlgorithmConfig algconf("mxaod_to_ntup_val");


  HG::runJob(algconf, argc, argv);
  
  return 0;
}
