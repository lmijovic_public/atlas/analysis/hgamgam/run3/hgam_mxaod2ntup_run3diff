# hgam_mxaod2ntup_run3diff

## About 

When analysing HGam MxAODs, one typically wants to construct some variables, and write them to smaller (no Athena Event Data Model) root outputs, which can be stored on a private disk-space, and analysed with plain root. 

Problem: code for procing microAODs, central to HGamCore: XSecSkimAndSlim, is designed to fetch simple, existing variables from MxAODs. It is less well suited for fetching containers, constructing new variables etc. 

Solution: this code, based on https://:@gitlab.cern.ch:8443/lmijovic_public/atlas/analysis/hgamgam/run3/hgam_mxaod2ntup_skeleton.git . Opens MxAOD, fetches containers, constructs variables & event weights, writes to output root file. Also handles cutflow hisograms - note: as per EventLoop design, these go to a separate output file (hist-sample.root)

## Install and run

Tested with: 
```
asetup AnalysisBase ,AnalysisBase,25.2.11
```

Needs to be built on top of HGamCore: https://atlas-hgam.docs.cern.ch/ Installation. 

Once you have downloaded and compiled HGamCore, get this package by doing: 
```
cd source # 
git clone https://:@gitlab.cern.ch:8443/lmijovic_public/atlas/analysis/hgamgam/run3/hgam_mxaod2ntup_run3diff.git
# after this, if you list source, you should see: 
# ls . 
# CMakeLists.txt  HGamCore  hgam_mxaod2ntup_run3diff
# now you're ready to build: 
cd ../build
cmake ../source 
# compile with 10 cores; lscpu to know how many you have 
make -j10  
```

To run:
```
cd ../run
source ../build/x*/setup.sh

# prepare the file to run over: 
echo /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h030/mc23d/mc23d.PhPy8_ggH125_NNLOPS.MxAODDetailedNoSkim.e8559_s4159_r15224_p6189_h030.root/group.phys-higgs.39629605._000001.MxAOD.root > sample.txt

# run like so: 
run_mxaod_to_ntup_baseline hgam_mxaod2ntup_run3diff/hgam_mxaod2ntup_run3diff.cfg
```

The output ntuple is:

```
hgam_mxaod2ntup_run3diff_DATE/data-MxAOD/sample.root 
```

Cutflow histograms:

```
hgam_mxaod2ntup_run3diff_DATE/hist-sample.root
```


In the run above, the hgam_mxaod2ntup_run3diff/hgam_mxaod2ntup_run3diff.cfg contains run-time configuration such as how many events you run over. 
In the out-of-the-box run above, you used the version in data/ directory, which is automatically available through make . If you want to modify the configu, you can get hold of it by:

```
# in run: 
cp ../source/hgam_mxaod2ntup_run3diff/data/hgam_mxaod2ntup_run3diff.cfg 
# then run with local copy; 
run_mxaod_to_ntup_baseline hgam_mxaod2ntup_run3diff.cfg
```


## Package creator script : optional 

In reality, your MxAOD analysis will be more specialised than fetching one container and storing one variable. I use this skeleton + renaming scripts to create staring points for new, more specialised MxAOD analysis packages. If you'd like to do this too, commands below. 

To start a package in a directory newdirname with files named newname:

* move the top directory: 

```  
cd source/
newdirname=test_xxx # whatever you want the name of your package to be 
mv hgam_mxaod2ntup_run3diff $newdirname
```
  
* edit make_package.sh : insert the new names of the new directory and  top of the script. when done, make it executable:
  
```
chmod +x make_package.sh
```


* run: 
```
./make_package.sh
```
    

If adding to git:
```
# git: 
# clear out the original git dir 
for d in $(find $PWD -type d -name '.git'); do echo $d; done 

# copy files over to empty git project, and add with:  
git rm *$oldname*
git rm *$olddirname*
git add *
```
