#include <hgam_mxaod2ntup_run3diff/mxaod_to_ntup_baseline.h>
#include <hgam_mxaod2ntup_run3diff/mxaod_to_ntup_val.h>
#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#endif
#ifdef __CINT__
#pragma link C++ class mxaod_to_ntup_baseline+;
#pragma link C++ class mxaod_to_ntup_val+;
#endif
