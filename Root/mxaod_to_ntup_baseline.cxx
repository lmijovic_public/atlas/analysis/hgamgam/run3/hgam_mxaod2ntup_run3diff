// 
// @author: liza.mijovic@cern.ch
#define XXX std::cout << "I am here: " << __FILE__ << ":" << __LINE__ << std::endl;


#include "hgam_mxaod2ntup_run3diff/mxaod_to_ntup_baseline.h"
#include "hgam_mxaod2ntup_run3diff/utils.h"

#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/HGamVariables.h"
#include "HGamAnalysisFramework/VarHandler.h"
#include "HGamAnalysisFramework/HGamCategoryTool.h"
#include "HGamAnalysisFramework/TruthHandler.h"

// this is needed to distribute the algorithm to the workers
ClassImp(mxaod_to_ntup_baseline)

mxaod_to_ntup_baseline::mxaod_to_ntup_baseline(const std::string &name,
                         ISvcLocator *pSvcLocator)
: HgammaAnalysis(name, pSvcLocator) {
  //nop
} // end of constructor 

mxaod_to_ntup_baseline::~mxaod_to_ntup_baseline() {
  // nop
} // end of desctructor 


EL::StatusCode mxaod_to_ntup_baseline::initialize()
{
  ANA_CHECK_SET_TYPE(EL::StatusCode);

  EL::StatusCode sc = EL::StatusCode::SUCCESS;
    
  // this sets global variables such as isMC and isData etc ; HG::isMC() and HG::isData()
  // sc = HgammaAnalysis::initialize();

  // data or MC?
  // nominally HGamCore's HG::isMC() is used to determine this.
  // HG::isMC uses metadata under the hood. Currently (h030) MxAOD metadata is corrupted,
  // and most values are not set  => therefore, we assess isData / isMC from file names
  // volatile, but best we can do. TODO: replace with HG::isMC() once MxAOD Metadata fixed
  m_isMC = utils::isMC(config());

  // inherits from AnaAlgorith => we can access worker 
  TFile* file = wk()->getOutputFile("MxAOD");
  
  m_output_tree = new TTree("OutTree","OutTree");
  m_output_tree->SetDirectory(file);

  // get list of files to process; achieved via config() inherted from HgammaAnalysis:
  // info which info we can get via config():
  config()->printDB();
  std::string void_input = "";
  std::string in_file_list = (std::string) config()->getStr("InputFileList", void_input);
  std::string in_file = (std::string) config()->getStr("InputFile", void_input);
  if (void_input == in_file_list && void_input == in_file) {
    std::cout << __FILE__ << " " << __LINE__ << "Got no files to process " << std::endl;
    return(EL::StatusCode::SUCCESS);
  }
  
 // fill cutflow histos for all files to process.
  // NB: these histograms dont go to sama outputfile as the tree;
  // eventloop puts them to submissiondir/hist-*.root 
  std::pair<TH1F,TH1F> cf_histos = utils::getCutflowHists(in_file_list, wk());
    
  // put histos to class variables, since they are needed for weights in execute too
  // raw event cout cut-flow, no weighting applied  
  m_CutFlow = (TH1F*) cf_histos.first.Clone();
  // event cutflow with MC weights; will be nullptr for data 
  m_CutFlow_noDalitz_weighted = (TH1F*) cf_histos.second.Clone();
  // writes histos to output file  
  sc = book(*m_CutFlow);
  if (m_isMC) {
    sc = book(*m_CutFlow_noDalitz_weighted);
  }


  //===========================================================================
  // Event weights 
  //===========================================================================
  m_initial_weights_sum = 1.;
    // fill cutflow histos for all files to process. Only needed for MC (weights)
  if (m_isMC) {
        // create initial sum of weights:
    if (nullptr == m_CutFlow_noDalitz_weighted) {
            std::cout << "Warning: null cutflow histo, hard-coding initial sum to "
              << m_initial_weights_sum << std::endl;
    }
    else {
            // Print cutflow for debug purposes
      std::cout << "Using cutflow histogram " << std::endl;
      m_CutFlow_noDalitz_weighted->Print("all");

            // sum of weights in the AOD = sum weights of events we produced 
      double _sum_weights_AOD = m_CutFlow_noDalitz_weighted->GetBinContent(1);
            // sum of weights in DAOD, typically equal to events in AOD,
      // unless we are skimming (removing events failing preselection) in AOD -> DAOD 
      double _sum_weights_DAOD = m_CutFlow_noDalitz_weighted->GetBinContent(2);
      // sum of weights in MxAOD = sum weights of events we process
            double _sum_weights_MxAOD = m_CutFlow_noDalitz_weighted->GetBinContent(3);
      	
      if ( std::numeric_limits<double>::min() < abs(_sum_weights_DAOD)){
	        // assumption: no events lost due to technical issues in production of AOD->DAOD, and DAOD->MxAOD
        // if events were lost, we'd have two options:
        // 1) no skimming: reset  m_initial_weights_sum to sum_weights_MxAOD
        // 2) skimming: 
        m_initial_weights_sum = _sum_weights_MxAOD * _sum_weights_AOD / _sum_weights_DAOD;
      }
      else {
        std::cout << "Warning: invalid sum of weights, hard-coding initial sum to "
                  << m_initial_weights_sum  << std::endl;
      }
      	}
      } // end isMC requirement for weights 
  
  //===========================================================================
  // Set up all output branches
  //===========================================================================
  m_output_tree->Branch( "cutFlow", &m_cutFlow );

  m_output_tree->Branch( "runNumber",&m_runNumber);
  m_output_tree->Branch( "eventNumber",&m_eventNumber);
  m_output_tree->Branch( "weight_MC", &m_weight_MC );
  m_output_tree->Branch( "weight" , &m_weight );
  m_output_tree->Branch( "weight_xsec_br_filter", &m_weight_xsec_br_filter );
  m_output_tree->Branch( "weight_total" , &m_weight_total );

  m_output_tree->Branch( "m_yy", &m_m_yy );
  
  m_output_tree->Branch( "y1_pt", &m_y1_pt );
  m_output_tree->Branch( "y1_E", &m_y1_E );
  m_output_tree->Branch( "y1_eta", &m_y1_eta );
  m_output_tree->Branch( "y1_phi", &m_y1_phi );
  m_output_tree->Branch( "y2_pt", &m_y2_pt );
  m_output_tree->Branch( "y2_E", &m_y2_E );
  m_output_tree->Branch( "y2_eta", &m_y2_eta );
  m_output_tree->Branch( "y2_phi", &m_y2_phi );
  m_output_tree->Branch( "y1_convType", &m_y1_convType );
  m_output_tree->Branch( "y2_convType", &m_y2_convType );

  // Lepton info 
  m_output_tree->Branch( "n_lep15", &m_n_lep15 );

  m_output_tree->Branch( "jet_pt", &m_jet_pt );
  m_output_tree->Branch( "jet_eta", &m_jet_eta );
  m_output_tree->Branch( "jet_phi", &m_jet_phi );
  m_output_tree->Branch( "jet_E", &m_jet_E );

  m_output_tree->Branch( "jetathena_pt", &m_jetathena_pt );
  m_output_tree->Branch( "jetathena_eta", &m_jetathena_eta );
  m_output_tree->Branch( "jetathena_phi", &m_jetathena_phi );
  m_output_tree->Branch( "jetathena_E", &m_jetathena_E );

  if (m_isMC) {
    m_output_tree->Branch( "truth_y1_pt", &m_truth_y1_pt );
    m_output_tree->Branch( "truth_y1_E", &m_truth_y1_E );
    m_output_tree->Branch( "truth_y1_eta", &m_truth_y1_eta );
    m_output_tree->Branch( "truth_y1_phi", &m_truth_y1_phi );
    
    m_output_tree->Branch( "truth_y2_pt", &m_truth_y2_pt );
    m_output_tree->Branch( "truth_y2_E", &m_truth_y2_E );
    m_output_tree->Branch( "truth_y2_eta", &m_truth_y2_eta );
    m_output_tree->Branch( "truth_y2_phi", &m_truth_y2_phi );

    m_output_tree->Branch( "truth_jet_pt", &m_truth_jet_pt );
    m_output_tree->Branch( "truth_jet_eta", &m_truth_jet_eta );
    m_output_tree->Branch( "truth_jet_phi", &m_truth_jet_phi );
    m_output_tree->Branch( "truth_jet_E", &m_truth_jet_E );
    
  }
  
  return sc;
}

//____________________________________________________________________________
template <class Container>
EL::StatusCode
mxaod_to_ntup_baseline::getContainer(Container* &cont, const std::string &name)
{
  ANA_CHECK_SET_TYPE( EL::StatusCode );

  const Container *constCont = nullptr;

  if (!m_outEvent->contains<Container>(name)) {
    ATH_MSG_ERROR("Couldn't retrieve " << name.c_str() << " from output event.");
    return EL::StatusCode::FAILURE;
  }

  ANA_CHECK( m_outEvent->retrieve(constCont, name) );

  cont = const_cast<Container*>(constCont);

  return EL::StatusCode::SUCCESS;
}




EL::StatusCode mxaod_to_ntup_baseline::execute()
{

  ANA_CHECK_SET_TYPE(EL::StatusCode);

  if( !clearStoredVariables() ) return EL::StatusCode::SUCCESS;

  //===========================================================================
  // Meta-data for eg run/train/test splits
  //===========================================================================
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  
  const xAOD::EventInfo *HGamEventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (HGamEventInfo, "HGamEventInfo"));
  
  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
  
  //===========================================================================
  // Highest cutflow;
  //===========================================================================
  //HGamEventInfoAuxDyn_cutFlow
  m_cutFlow = HGamEventInfo->auxdataConst<int>("cutFlow");

  //===========================================================================
  // Event weights for MC normalization and debugging
  //===========================================================================
  m_weight_MC = 1.;
  m_weight = 1.;
  m_weight_xsec_br_filter = 1.;
  m_weight_total = 1.;

  if (m_isMC) {
    std::vector<float> _all_weights =
      eventInfo->auxdataConst< std::vector<float> >("mcEventWeights") ;
    if (_all_weights.size()>0) {
      m_weight_MC = _all_weights[0];
    }
    m_weight = HGamEventInfo->auxdataConst<float>("weight");
    m_weight_xsec_br_filter = HGamEventInfo->auxdataConst<float>("crossSectionBRfilterEff");
    // per-event weight including normalization to 1 ifb ilumi
    float _lumi_pb = 1.*1000;
    // set weight up to requirements valid at this stage.
    // NB: this gets updated below, depending on full event selection 
    m_weight_total = m_weight * m_weight_xsec_br_filter * _lumi_pb / m_initial_weights_sum;
  } // end of isMC condition for MC weights calc 


  // -------------------------------------------------------------------
  //   Leptons
  // -------------------------------------------------------------------
  m_n_lep15 = HGamEventInfo->auxdataConst<int>("N_lep_15");

  // -------------------------------------------------------------------
  //   Leptons
  // -------------------------------------------------------------------
  m_m_yy = HGamEventInfo->auxdataConst<float>("m_yy")/1000.;

  
  // -------------------------------------------------------------------
  //   Photons 
  // -------------------------------------------------------------------

  const xAOD::PhotonContainer* photons_all = nullptr;
  ANA_CHECK (evtStore()->retrieve (photons_all, "HGamPhotons"));

  
  // loop over reco photons: 
  // Which methods does the photon have: 
  // https://gitlab.cern.ch/atlas/athena/-/blob/main/Event/xAOD/xAODEgamma/xAODEgamma/versions/Egamma_v1.h
  // https://gitlab.cern.ch/atlas/athena/-/blob/main/Event/xAOD/xAODEgamma/xAODEgamma/versions/Photon_v1.h
  int i=0;
  for (auto photon : *photons_all){
    ++i;

    // connversion type:
    int convType = photon->auxdataConst<int>("conversionType");
    // access directly, since photon->conversionType() returns 0
    // https://gitlab.cern.ch/atlas/athena/-/blob/main/Event/xAOD/xAODEgamma/xAODEgamma/EgammaEnums.h

    if (1==i){
      m_y1_pt = photon->pt()/1000.;
      m_y1_eta = photon->eta();
      m_y1_phi = photon->phi();
      m_y1_E = photon->e()/1000.;
      m_y1_convType = convType;
    }
    else {
      m_y2_pt = photon->pt()/1000.;
      m_y2_eta = photon->eta();
      m_y2_phi = photon->phi();
      m_y2_E = photon->e()/1000.;
      m_y2_convType = convType;
    }
  }

  const xAOD::JetContainer* jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (jets, "HGamAntiKt4EMPFlowCustomVtxJets"));

  // jvt alread applied in h030 , and uses different vars in h029 
  //static SG::AuxElement::Accessor<char> acc_passJVT("passJVT");
  //static SG::AuxElement::Accessor<char> acc_passFJVT("passFJVT");

  for( auto jet : *jets ) {
    m_jet_pt.push_back(jet->pt()/1000.);
    m_jet_E.push_back(jet->e()/1000.);
    m_jet_eta.push_back(jet->eta());
    m_jet_phi.push_back(jet->phi());
    //if (acc_passJVT(*jet)) {
    //  std::cout << jet->pt() << " , passJVT " << std::endl;
    // }
  }

  const xAOD::JetContainer* jets_athena = nullptr;
  ANA_CHECK (evtStore()->retrieve (jets_athena, "HGamAntiKt4EMPFlowJets"));
  for( auto jet : *jets_athena ) {
    m_jetathena_pt.push_back(jet->pt()/1000.);
    m_jetathena_E.push_back(jet->e()/1000.);
    m_jetathena_eta.push_back(jet->eta());
    m_jetathena_phi.push_back(jet->phi());
  }


  if (m_isMC) {

    const xAOD::TruthParticleContainer* photons_truth = nullptr;
    ANA_CHECK (evtStore()->retrieve (photons_truth, "HGamTruthPhotons"));

    i=0;
    for (auto photon : *photons_truth){
      ++i;
      if (1==i){
        m_truth_y1_pt = photon->pt()/1000.;
        m_truth_y1_eta = photon->eta();
        m_truth_y1_phi = photon->phi();
        m_truth_y1_E = photon->e()/1000.; 
      }
      else {
        m_truth_y2_pt = photon->pt()/1000.;
        m_truth_y2_eta = photon->eta();
        m_truth_y2_phi = photon->phi();
        m_truth_y2_E = photon->e()/1000.;      
      }
    }
    
    const xAOD::JetContainer* jets_truth = nullptr;
    ANA_CHECK (evtStore()->retrieve (jets_truth, "HGamAntiKt4TruthWZJets"));

    i=0;
    for (auto jet : *jets_truth){
      m_truth_jet_pt.push_back(jet->pt()/1000.);
      m_truth_jet_E.push_back(jet->e()/1000.);
      m_truth_jet_eta.push_back(jet->eta());
      m_truth_jet_phi.push_back(jet->phi());
    }

  } // end of m_isMC condition 

  m_output_tree->Fill();
  
  return EL::StatusCode::SUCCESS;

} // end of EL::StatusCode mxaod_to_ntup_baseline::execute()

EL::StatusCode mxaod_to_ntup_baseline::finalize()
{

  return EL::StatusCode::SUCCESS;

} // end of EL::StatusCode mxaod_to_ntup_baseline::finalize()

bool mxaod_to_ntup_baseline::clearStoredVariables( ) {

  m_cutFlow = -1;

  m_runNumber = -999 ; 
  m_eventNumber = 999 ; 

  m_weight_MC = 1.0 ;
  m_weight = 1.0; 
  m_weight_xsec_br_filter = 1.0 ; 
  m_weight_total = 1.0 ;

  m_m_yy = -999 ;
  m_y1_pt = -999 ; 
  m_y2_pt = -999 ;
  m_y1_E = -999 ; 
  m_y2_E = -999 ; 
  m_y1_eta = -999 ; 
  m_y2_eta = -999 ; 
  m_y1_phi = -999 ; 
  m_y2_phi = -999 ;
  m_y1_convType = -1 ;
  m_y2_convType = -1 ;

  m_n_lep15 = 0 ;

  m_jet_pt.clear() ;
  m_jet_eta.clear() ; 
  m_jet_phi.clear() ; 
  m_jet_E.clear() ;

  m_jetathena_pt.clear() ;
  m_jetathena_eta.clear() ; 
  m_jetathena_phi.clear() ; 
  m_jetathena_E.clear() ;

  m_truth_y1_pt=-999;
  m_truth_y1_E=-999;
  m_truth_y1_eta=-999;
  m_truth_y1_phi=-999;
  m_truth_y2_pt=-999;
  m_truth_y2_E=-999;
  m_truth_y2_eta=-999;
  m_truth_y2_phi=-999;

  m_truth_jet_pt.clear();
  m_truth_jet_eta.clear();
  m_truth_jet_phi.clear();
  m_truth_jet_E.clear();

  return true;
}

