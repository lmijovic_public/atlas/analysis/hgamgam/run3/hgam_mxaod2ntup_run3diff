#include "hgam_mxaod2ntup_run3diff/utils.h"

#include "TFile.h"
#include "TKey.h"

#include<iostream>


// misc helpers, likely to be utilised across ntuple-making use-cases
namespace utils {


  std::pair<TH1F,TH1F> getCutflowHists(std::string p_input_file_list, EL::IWorker* p_wk) {

    TH1F dummy_cutflow("","",20,0,20);    
	
    TH1F* h_CutFlow = nullptr;
    TH1F* h_CutFlow_noDalitz_weighted = nullptr;
    
    // we assume single file passed directly as run time argument
    if (""==p_input_file_list) {

      TIter _next(p_wk->inputFile()->GetListOfKeys());
      TObject *_key = nullptr;
      
      while ((_key = _next())) {
	auto _tkey = (TKey *)_key;
	TString _name = _tkey->GetName();
	if (_name.Contains("CutFlow_")) {
	  TH1F* _th = (TH1F*) p_wk->inputFile()->Get(_name);
	  if (_name.Contains("_noDalitz_weighted")) {
	    h_CutFlow_noDalitz_weighted = _th;
	  }
	  else if (!_name.Contains("_noDalitz") && !_name.Contains("_weighted")){
	    h_CutFlow = _th;
	  }
	}      
      }  
      if (_key!=nullptr) {
	delete _key;
      }
      return std::pair<TH1F,TH1F>{dummy_cutflow,dummy_cutflow};
      
    }// end of ""==p_input_file_list


    // input file list
    std::vector<std::string> _in_files {};
    std::string _line;
    std::ifstream _sfile (p_input_file_list.c_str());
    if (_sfile.is_open()) {
      while ( getline (_sfile,_line) ) {
	_in_files.push_back(_line);
      }
      _sfile.close();
    }
    else {
      std::cout << __FILE__ << " " << __LINE__ << "warning, could not open input file : " << p_input_file_list << std::endl;
    }
  
    for (auto _file : _in_files) {
      TFile* _tf = TFile::Open(_file.c_str(), "READ");
      if (!_tf->IsOpen()) {
	std::cout << "warning, could not open " << _file << std::endl;
	std::cout << " ... skipping " << std::endl;
	continue;
      }
      TIter _next(_tf->GetListOfKeys());
      TObject *_key = nullptr;
      while ((_key = _next())) {
	auto _tkey = (TKey *)_key;
	TString _name = _tkey->GetName();
	if (_name.Contains("CutFlow_")) {
	  if (_name.Contains("_noDalitz_weighted")) {
	    TH1F* _th = (TH1F*) _tf->Get(_name);
	    if (!h_CutFlow_noDalitz_weighted) {
	      h_CutFlow_noDalitz_weighted = (TH1F*) _th->Clone();
	    }
	    else {
	      h_CutFlow_noDalitz_weighted->Add(_th);
	    }
	  }
	  else if (!_name.Contains("_noDalitz") && !_name.Contains("_weighted")){
	    TH1F* _th = (TH1F*) _tf->Get(_name);
	    if (!h_CutFlow) {
	      h_CutFlow = (TH1F*) _th->Clone();
	    }
	    else {
	      h_CutFlow->Add(_th);
	    }
	  }
	}
      }// end of loop over keys
      delete _key;
      //_tf->Close(); makes the histo pointer dangling ... 
    }// end of loop over files

    // return cutflows: 
    if (h_CutFlow==nullptr){
      h_CutFlow=(TH1F*)dummy_cutflow.Clone();
    }
    if (h_CutFlow_noDalitz_weighted==nullptr){
      h_CutFlow_noDalitz_weighted=(TH1F*)dummy_cutflow.Clone();
    }
    
    return(std::pair<TH1F,TH1F>{*h_CutFlow,*h_CutFlow_noDalitz_weighted});
    
  } // end of getCutflowHists


  
  // data or MC?
  // needed to replace HG::isMC() if MxAOD metadata is broken;
  // => in this we parse input file name(s) for "mc2X" and data strings
  bool isMC(HG::Config* p_cfg) {

    bool ret_isMC=true;
    
    // return this if varible is not set in config 
    std::string in_default = "";
    
    // we have two ways to set files to process; 
    // processing single file: "InputFile" key 
    std::string in_file_name = (std::string) p_cfg->getStr("InputFile", in_default);
    
    if (in_default == in_file_name) {
    // processing several files: "InputFileList" key
      std::string in_file_list = (std::string) p_cfg->getStr("InputFileList", in_default);
      if (in_default == in_file_list ){
	std::cout << __FILE__ << " " << __LINE__ << " warning, no valid input files specified " << std::endl;
	return(ret_isMC);
      }
      std::string line;
      std::ifstream sfile (in_file_list.c_str());
      if (sfile.is_open()) {
	while ( getline (sfile,line) ) {
	  if (line!="") {
	    in_file_name=line;
	    break;
	  }
	}
	sfile.close();
      }
      else {
	std::cout << __FILE__ << " , " << __LINE__
		  << " warning, could not open input file : " << in_file_list << std::endl;
	return(ret_isMC);
      }      
    }

    // We'll be processing mc2X or data* samples => MC contains mc2* string
    ret_isMC = in_file_name.find("mc2") != std::string::npos;

    // check if sample name indicates data or MC w/o ambiguity     
    if (!ret_isMC) {
      if (in_file_name.find("data") == std::string::npos){
	std::cout << __FILE__ << " , " << __LINE__
		  << " warning, not clear if file is data or MC, assuming MC : " << in_file_name << std::endl;
      }
      else ret_isMC = false;
    }
    
    return (ret_isMC); 
    
  } // end of isMC(...) 

  
  void test(EL::IWorker* p_wk){
    
    std::cout << "LM test " << std::endl;
    if (p_wk != nullptr) {
      std::string ifn = p_wk->inputFileName();
      std::cout << ifn << std::endl;
    }
    
  }// end of test


  
} // end of namespace utils

