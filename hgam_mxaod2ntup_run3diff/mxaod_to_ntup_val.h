// @author: liza.mijovic@cern.ch

#ifndef HGAM_MXAOD2NTUP_RUN3DIFF_MXAOD_TO_NTUP_VAL_H
#define HGAM_MXAOD2NTUP_RUN3DIFF_MXAOD_TO_NTUP_VAL_H

#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "HGamAnalysisFramework/HGamCommon.h"

#include <fstream>
#include "vector"
#include <memory>

class mxaod_to_ntup_val : public HgammaAnalysis
{

public:
  // this is a standard constructor
  mxaod_to_ntup_val(const std::string &name, ISvcLocator *pSvcLocator);
  virtual ~mxaod_to_ntup_val();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode initialize();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode finalize();

  template <class Container>
  EL::StatusCode
  getContainer(Container *&cont, const std::string &name);

private:  

  //====================================================================
  // Output ntuple
  //====================================================================
  //--------------------------------------------------------------------
  bool clearStoredVariables();

  std::unique_ptr<xAOD::TEvent> m_outEvent; //! 
  TTree *m_output_tree; //!

  //===========================================================================
  // Cutflows for event weight and debugging
  //===========================================================================
  // write total cuflow histograms of all processed files
  // these need to be pointers due to event-loops memo management;
  // it attempts to delete all (pointer or non-pointer) histos in finalize
  TH1F* m_CutFlow; //!
  TH1F* m_CutFlow_noDalitz_weighted; //!

  //===========================================================================
  // Highest cutflow;
  // draw m_CutFlow and check x-axisfor meaning of this number;
  // >=15 imples passes all selections   
  //===========================================================================
  //HGamEventInfoAuxDyn_cutFlow
  int m_cutFlow; //!
  
  //===========================================================================
  // Meta-data for eg run/train/test splits 
  //===========================================================================
  unsigned int m_runNumber ; //!
  ULong64_t m_eventNumber ; //!
  
  //===========================================================================
  // Event weights for MC normalization and debugging 
  //===========================================================================
  // https://atlas-hgam.docs.cern.ch/Weights/
  // intermediary weights:
  // total sum of weights 
  float m_initial_weights_sum;
  // MC gen nominal weight 
  float m_weight_MC; //!
  // di-photon selection * pu * MCgen * vertex 
  float m_weight;
  // theory xs * BR 
  float m_weight_xsec_br_filter ; //!
  // actual weight to apply to MC events, for norm to ilumi = 1fb 
  float m_weight_total ; //!
  
  //===========================================================================
  // Switches 
  //===========================================================================
  bool m_isMC;

  //===========================================================================
  // Reco & truth objects 
  //===========================================================================
  // reco photons 
  double m_y1_pt ; //!
  double m_y1_E ; //!
  double m_y1_eta ; //!
  double m_y1_phi ; //!
  int m_y1_convType ; //!
  int m_y1_isTight ; //!
  double m_y1_ptcone20; //!
  double m_y1_topoetcone40; //!
  
  double m_y2_pt ; //!
  double m_y2_E ; //!
  double m_y2_eta ; //!
  double m_y2_phi ; //!
  int m_y2_convType ; //!
  int m_y2_isTight ; //!
  double m_y2_ptcone20; //!
  double m_y2_topoetcone40; //!
  
  // reco jet info
  // HGam PV jets 
  std::vector<double> m_jet_pt ; //!
  std::vector<double> m_jet_eta ; //!
  std::vector<double> m_jet_phi ; //!
  std::vector<double> m_jet_E ; //!
  std::vector<double> m_jet_jvt; //!
  std::vector<double> m_jet_fjvt; //! 
  
  // hardest ptsum PV jets 
  std::vector<double> m_jetathena_pt ; //!
  std::vector<double> m_jetathena_eta ; //!
  std::vector<double> m_jetathena_phi ; //!
  std::vector<double> m_jetathena_E ; //!
  std::vector<double> m_jetathena_jvt; //!
  std::vector<double> m_jetathena_fjvt; //!

  // event-level observables:
  int m_n_lep15  ; //!
  int m_N_j_btag30  ; //!
  int m_N_j_30; //!
  double m_m_jj_30; //!
  double m_met_TST; //!
  
  // truth photons
  double m_truth_y1_pt; //!
  double m_truth_y1_E; //!
  double m_truth_y1_eta; //!
  double m_truth_y1_phi; //!
  double m_truth_y2_pt; //!
  double m_truth_y2_E; //!
  double m_truth_y2_eta; //!
  double m_truth_y2_phi; //!

  // Truth Jet info
  std::vector<float> m_truth_jet_pt; //!
  std::vector<float> m_truth_jet_eta; //!
  std::vector<float> m_truth_jet_phi; //!
  std::vector<float> m_truth_jet_E; //!
  
  // this is needed to distribute the algorithm to the workers
  ClassDef(mxaod_to_ntup_val, 1);
   
};

#endif // HGAM_MXAOD2NTUP_RUN3DIFF_MXAOD_TO_NTUP_VAL_H
