// @author: liza.mijovic@cern.ch

#ifndef UTILS_H
#define UTILS_H

#include "HGamAnalysisFramework/Config.h"
#include "EventLoop/IWorker.h"

#include "TH1F.h"

#include <fstream>

namespace utils {

  void test(EL::IWorker* p_wk);

  // data or MC?
  // needed to replace HG::isMC() when MxAOD metadata is broken 
  bool isMC(HG::Config* p_cfg); 

  std::pair<TH1F,TH1F> getCutflowHists(std::string p_input_file_list, EL::IWorker* p_wk);
  
}

#endif // UTILS_H
